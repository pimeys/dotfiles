#!/usr/bin/env bash
set -eu

[[ -z "$(pgrep swaylock)" ]] || exit
[[ -z "$(pgrep convert)" ]] || exit
[[ -z "$(pgrep grim)" ]] || exit

TMPBG=/tmp/bg.png

grim $TMPBG
convert $TMPBG -scale 10% -scale 1000% $TMPBG
swaylock -i $TMPBG
rm -rf $TMPBG
