set fisher_home ~/.local/share/fisherman
set fisher_config ~/.config/fisherman
source $fisher_home/config.fish
rvm default
set PATH ~/.cargo/bin ~/.bin ~/.local/bin ~/scripts /snap/bin $PATH
set GOPATH ~/.local
export LANG=en_US.UTF-8
export EDITOR=vim

# OPAM configuration
source /home/pimeys/.opam/opam-init/init.fish > /dev/null 2> /dev/null or true

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/pimeys/Downloads/google-cloud-sdk/path.fish.inc' ]; if type source > /dev/null; source '/home/pimeys/Downloads/google-cloud-sdk/path.fish.inc'; else; . '/home/pimeys/Downloads/google-cloud-sdk/path.fish.inc'; end; end
