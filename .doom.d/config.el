;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here

(setq doom-font (font-spec :family "Inconsolata for Powerline" :size 18))

(setq lsp-ui-flycheck-enable nil)
(setq lsp-ui-sideline-show-flycheck t)

(with-eval-after-load 'rust-mode
                      (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))
