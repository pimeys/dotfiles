#!/bin/bash

cwd=$(pwd)

ln -s $cwd/.config/* $HOME/.config/
ln -s $cwd/.i3 $HOME/
ln -s $cwd/scripts $HOME/
ln -s $cwd/.Xresources $HOME/
ln -s $cwd/.tmux.conf $HOME/
ln -s $cwd/.compton.conf $HOME/
ln -s $cwd/.doom.d $HOME/
