#!/usr/bin/python3

import requests
import json
import time
import datetime
import dateutil.relativedelta
import toml

from pathlib import Path

config_location = str(Path.home()) + "/.config/glucose/secret.toml"
config          = toml.loads(open(config_location).read())
headers         = {"API-SECRET": config['secret']['api_secret']}
api_uri         = "{0}/api/v1/entries/current.json".format(config['secret']['api_uri'])

response = requests.get(api_uri, headers=headers, timeout=2.0).json()
glucose  = round(response[0]["sgv"] / 18.018018, 1)
delta    = round(response[0]["delta"] / 18.018018, 1)

dt1 = datetime.datetime.fromtimestamp(response[0]["date"] / 1000.0)
dt2 = datetime.datetime.fromtimestamp(time.time())

duration = dateutil.relativedelta.relativedelta(dt2, dt1)

if duration.hours > 0:
    print("{0} mmol/l Δ {1} ⌛ ({2}h {3}m)".format(glucose, delta, duration.hours, duration.minutes))
else:
    print("{0} mmol/l Δ {1} ⌛ ({2}m)".format(glucose, delta, duration.minutes))
